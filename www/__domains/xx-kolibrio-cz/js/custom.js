var body;
var initialized = false;

$(function () {
    body = $("body");

    initAll();
});


var initAll = function () {
    if (!initialized) {
        initOnce();
        magnificPopupLanguage();

        initialized = true;
    }

    initAlways();
}


var initOnce = function () {
    tableCustom();
}


var initAlways = function () {

}

var tableCustom = function(){
    if($(".content table").length) {
        $(".content table").each(function (index) {
            $(this).addClass("table table-striped bdr-bottom").wrap("<div class='table-responsive'>");
        });
    }
}

var magnificPopupLanguage = function(){

    if ($("html .lang[data-lang='cs']").length){

        $.extend(true, $.magnificPopup.defaults, {
            gallery: {
                tPrev: 'Zpět',
                tNext: 'Další',
                tCounter: '%curr% z %total%'
            },
            tClose: 'Zavřít (Esc)',
            tLoading: 'Načítám...'
        });
    }
}

